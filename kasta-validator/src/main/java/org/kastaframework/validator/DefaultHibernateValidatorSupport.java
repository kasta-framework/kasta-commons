package org.kastaframework.validator;

import org.kastaframework.core.exception.ExceptionFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Set;

/**
 * Default hibernate validator support.
 *
 * @author Sabri Onur Tuzun
 * @since 25.10.2013 15:13
 */
public class DefaultHibernateValidatorSupport implements HibernateValidatorSupport {

    private ExceptionFactory exceptionFactory;

    /**
     * Constructor with exception factory.
     *
     * @param exceptionFactory exceptionFactory
     */
    public DefaultHibernateValidatorSupport(ExceptionFactory exceptionFactory) {
        this.exceptionFactory = exceptionFactory;
    }

    /**
     * Validates object.
     *
     * @param object object
     * @param <T>    objectClass
     * @return constraint violations
     */
    @Override
    public <T extends Validatable> Set<ConstraintViolation<T>> validate(T object) {
        return Validation.buildDefaultValidatorFactory()
                .getValidator()
                .validate(object);
    }

    /**
     * Validates object and throw passed exception if any error is occured.
     *
     * @param object             object
     * @param exceptionClass     exception class
     * @param exceptionArguments exception arguments
     * @param <T>                objectClass
     * @param <E>                validation exception class
     * @throws ValidationException
     */
    @Override
    public <T extends Validatable, E extends ValidationException> void validate(T object, Class<E> exceptionClass, Object... exceptionArguments) throws E {
        Set<ConstraintViolation<T>> violations = validate(object);
        exceptionFactory.throwIfNotEmpty(violations, exceptionClass, exceptionArguments);
    }
}
