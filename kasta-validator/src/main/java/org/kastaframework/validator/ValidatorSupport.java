package org.kastaframework.validator;

/**
 * Base validator support interface.
 *
 * @author Sabri Onur Tuzun
 * @since 25.10.2013 17:03
 */
public interface ValidatorSupport extends ThrowableValidatorSupport {

    /**
     * Validates object.
     *
     * @param object object
     * @param <T>    objectClass
     * @return result
     */
    <T extends Validatable> Object validate(T object);
}
