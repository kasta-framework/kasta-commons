package org.kastaframework.validator;

import org.kastaframework.core.exception.BaseException;

/**
 * Base validation exception.
 *
 * @author Sabri Onur Tuzun
 * @since 24.10.2013 17:42
 */
public class ValidationException extends BaseException {

    /**
     * Construct with message.
     *
     * @param message message
     */
    public ValidationException(String message) {
        super(message);
    }
}
