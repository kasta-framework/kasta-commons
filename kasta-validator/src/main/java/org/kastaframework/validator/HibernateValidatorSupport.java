package org.kastaframework.validator;

import javax.validation.ConstraintViolation;
import java.util.Set;

/**
 * Hibernate validator support.
 *
 * @author Sabri Onur Tuzun
 * @since 25.10.2013 16:58
 */
public interface HibernateValidatorSupport extends ValidatorSupport {

    /**
     * Validates object and return violations.
     *
     * @param object object
     * @param <T>    objectClass
     * @return violations
     */
    @Override
    <T extends Validatable> Set<ConstraintViolation<T>> validate(T object);
}
