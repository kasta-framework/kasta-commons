package org.kastaframework.validator;

/**
 * Validates object.
 *
 * @author Sabri Onur Tuzun
 * @since 24.10.2013 17:41
 */
public interface Validator<T extends Validatable> {

    /**
     * Validates object.
     *
     * @param object object
     */
    void validate(T object);
}
