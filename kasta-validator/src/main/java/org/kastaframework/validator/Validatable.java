package org.kastaframework.validator;

/**
 * Marker validatable interface.
 *
 * @author Sabri Onur Tuzun
 * @since 30.07.2014 11:24
 */
public interface Validatable {
}
