package org.kastaframework.validator;

/**
 * Throwable validator support.
 *
 * @author Sabri Onur Tuzun
 * @since 29.10.2013 16:18
 */
public interface ThrowableValidatorSupport {

    /**
     * Validates object and throw exception if needed.
     *
     * @param object             object
     * @param exceptionClass     exception class
     * @param exceptionArguments exception arguments
     * @param <T>                validatable object class
     * @param <E>                type
     * @throws E
     */
    <T extends Validatable, E extends ValidationException> void validate(T object, Class<E> exceptionClass, Object... exceptionArguments) throws E;
}
