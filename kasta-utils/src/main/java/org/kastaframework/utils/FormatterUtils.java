package org.kastaframework.utils;

import java.util.Formatter;

/**
 * Formatter utility.
 *
 * @author Sabri Onur Tuzun
 * @since 02.12.2013 10:39
 */
public final class FormatterUtils {

    /**
     * Private constructor.
     */
    private FormatterUtils() {
    }

    /**
     * Formats given string.
     *
     * @param str       formattable string
     * @param arguments arguments
     * @return formatted string
     */
    public static String format(String str, Object... arguments) {
        StringBuilder formattedString = new StringBuilder();
        Formatter formatter = new Formatter(formattedString);
        formatter.format(str, arguments);
        return formattedString.toString();
    }
}
