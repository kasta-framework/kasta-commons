package org.kastaframework.utils;

import org.apache.commons.lang3.ClassUtils;

/**
 * Handy utility for class loading.
 *
 * @author Greg Luck
 * @author Sabri Onur Tuzun
 */
@SuppressWarnings("unchecked")
public final class ClassHelper {

    /**
     * Utility class.
     */
    private ClassHelper() {
    }

    /**
     * Gets the <code>ClassLoader</code> that all classes in ehcache, and extensions, should
     * use for classloading. All ClassLoading in ehcache should use this one. This is the only
     * thing that seems to work for all of the class loading situations found in the wild.
     *
     * @return the thread context class loader.
     */
    public static ClassLoader getStandardClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    /**
     * Gets a fallback <code>ClassLoader</code> that all classes in ehcache, and extensions,
     * should use for classloading. This is used if the context class loader does not work.
     *
     * @return the <code>ClassLoaderUtil.class.getClassLoader();</code>
     */
    public static ClassLoader getFallbackClassLoader() {
        return ClassHelper.class.getClassLoader();
    }

    /**
     * Creates a new class instance. Logs errors along the way. Classes are loaded using the
     * ehcache standard classloader.
     *
     * @param className a fully qualified class name
     * @return null if the instance cannot be loaded
     */
    private static Object createNewInstance(String className) {
        Class clazz;
        Object newInstance;

        try {
            clazz = Class.forName(className, true, getStandardClassLoader());
        } catch (ClassNotFoundException e) {
            // try fallback
            try {
                clazz = Class.forName(className, true, getFallbackClassLoader());
            } catch (ClassNotFoundException ex) {
                throw new RuntimeException("Unable to load class " +
                        className + ". Initial cause was " + e.getMessage(), e);
            }
        }

        try {
            newInstance = clazz.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("Unable to load class " +
                    className + ". Initial cause was " + e.getMessage(), e);
        }
        return newInstance;
    }

    /**
     * Casts object.
     *
     * @param value        value
     * @param classOfValue class of value
     * @param <T>          type
     * @return casted object
     */
    public static <T> T cast(Object value, Class<T> classOfValue) {
        return ClassUtils.isAssignable(value.getClass(), classOfValue) ? (T) value : null;
    }
}
