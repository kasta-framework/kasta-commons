package org.kastaframework.utils;

import java.util.Date;

/**
 * Utility date helper class.
 *
 * @author Sabri Onur Tuzun
 * @since 25.09.2013 08:56
 */
public final class DateHelper {

    /**
     * Constructor.
     */
    private DateHelper() {
    }

    /**
     * Returns current date.
     *
     * @return now
     */
    public static Date now() {
        return new Date();
    }
}
