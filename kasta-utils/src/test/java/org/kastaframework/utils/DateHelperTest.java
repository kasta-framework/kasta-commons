package org.kastaframework.utils;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Date;

/**
 * Date helper test.
 *
 * @author Sabri Onur Tuzun
 * @since 18.09.2013 19:20
 */
public class DateHelperTest {

    @Test
    public void testIfPrivateConstructorIsAccessible() throws Exception {
        Constructor constructor = DateHelper.class.getDeclaredConstructor();
        Assert.assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void testIfReturnedDateEqualsToNow() {
        Date date = DateHelper.now();
        Assert.assertNotNull(date);
        Assert.assertEquals(new Date().toString(), date.toString());
    }
}
