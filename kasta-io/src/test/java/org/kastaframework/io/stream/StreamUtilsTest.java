package org.kastaframework.io.stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import static org.mockito.Mockito.when;

/**
 * @author Sabri Onur Tuzun
 * @since 25.09.2013 08:48
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(IOUtils.class)
public class StreamUtilsTest {

    @Mock
    InputStream inputStream;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testIfPrivateConstructorIsAccessible() throws Exception {
        Constructor constructor = StreamingHelper.class.getDeclaredConstructor();
        Assert.assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void testWithNullInputStream() {
        String str = StreamingHelper.toString(null);
        Assert.assertEquals(StringUtils.EMPTY, str);
    }

    @Test
    public void testExceptionalToString() throws Exception {
        PowerMockito.mockStatic(IOUtils.class);

        when(IOUtils.toString(inputStream)).thenThrow(new IOException());

        String str = StreamingHelper.toString(inputStream);
        Assert.assertEquals(StringUtils.EMPTY, str);

        PowerMockito.verifyStatic();
        IOUtils.toString(inputStream);
    }
}
