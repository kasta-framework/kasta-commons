package org.kastaframework.io;

import java.lang.reflect.Method;

/**
 * @author Sabri Onur Tuzun
 * @since 24.04.2014 13:53
 */
public interface ReflectionHelper {

    /**
     * Invokes method.
     *
     * @param method method
     * @param target target
     * @param args   args
     * @return value
     */
    Object invokeMethod(Method method, Object target, Object... args);

    /**
     * Finds method.
     *
     * @param clazz      class
     * @param name       name
     * @param paramTypes parameter types
     * @param <T>        type
     * @return method
     */
    <T> Method findMethod(Class<T> clazz, String name, Class<?>... paramTypes);
}
