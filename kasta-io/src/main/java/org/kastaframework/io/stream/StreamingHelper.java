package org.kastaframework.io.stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Streaming utility class.
 *
 * @author Sabri Onur Tuzun
 * @since 23.09.2013 17:02
 */
public final class StreamingHelper {

    private static final Logger logger = LoggerFactory.getLogger(StreamingHelper.class);

    /**
     * Constructor.
     */
    private StreamingHelper() {
    }

    /**
     * Converts inputstream to string. If an exception is occured
     * empty string will be returned.
     *
     * @param inputStream inputstream
     * @return string data
     */
    public static String toString(InputStream inputStream) {
        String data = StringUtils.EMPTY;
        try {
            if (inputStream != null) {
                data = IOUtils.toString(inputStream);
            }
        } catch (IOException e) {
            logger.error("Caught exception while " +
                    "toString() operation of inputStream", e);
        }
        return data;
    }
}
