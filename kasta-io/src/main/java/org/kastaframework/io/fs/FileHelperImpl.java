package org.kastaframework.io.fs;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;

/**
 * File helper implementation.
 *
 * @author Sabri Onur Tuzun
 * @since 11/12/14 2:48 PM
 */
public class FileHelperImpl implements FileHelper {

    /**
     * Deletes file quietly.
     *
     * @param path path
     */
    @Override
    public void deleteFile(String path) {
        FileUtils.deleteQuietly(new File(path));
    }

    /**
     * Deletes file which is under parent file path.
     *
     * @param parent parent
     * @param path   path
     */
    @Override
    public void deleteFile(String parent, String path) {
        FileUtils.deleteQuietly(new File(parent, path));
    }

    /**
     * Writes byte array to a file.
     *
     * @param path  path
     * @param bytes bytes
     */
    @Override
    public void writeFile(String path, byte[] bytes) {
        try {
            FileUtils.writeByteArrayToFile(new File(path), bytes);
        } catch (Exception e) {
            throw new WriteFileException("Caught exception while writing byte array to a file", e);
        }
    }

    /**
     * Copies temp file to dest directory path.
     *
     * @param tempFilePath tempFilePath
     * @param destDirPath  destDirPath
     */
    @Override
    public void copyFileToDirectory(String tempFilePath, String destDirPath) {
        try {
            FileUtils.copyFileToDirectory(new File(tempFilePath), new File(destDirPath));
        } catch (IOException e) {
            throw new WriteFileException("Caught exception while copying file to destination dir", e);
        }
    }

    /**
     * Copies temp file to dest file path.
     *
     * @param tempFilePath tempFilePath
     * @param destFilePath destFilePath
     */
    @Override
    public void copyFileToFile(String tempFilePath, String destFilePath) {
        try {
            FileUtils.copyFile(new File(tempFilePath), new File(destFilePath));
        } catch (IOException e) {
            throw new WriteFileException("Caught exception while copying file to destination file", e);
        }
    }

    /**
     * Appends extension to destination file path and copies it.
     *
     * @param tempFilePath tempFilePath
     * @param destBasePath destBasePath
     * @param destFileName destFileName
     * @return dest file
     */
    @Override
    public String appendExtensionAndCopyFileToFile(String tempFilePath, String destBasePath, String destFileName) {
        String extension = getFileExtension(tempFilePath);
        String destFile = destFileName + extension;
        copyFileToFile(tempFilePath, destBasePath + destFile);
        return destFile;
    }

    /**
     * Returns file extension.
     *
     * @param path path
     * @return file extension
     */
    @Override
    public String getFileExtension(String path) {
        return '.' + FilenameUtils.getExtension(path);
    }
}
