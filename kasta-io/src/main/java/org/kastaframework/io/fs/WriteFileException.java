package org.kastaframework.io.fs;

import org.kastaframework.core.exception.BaseException;

/**
 * @author Sabri Onur Tuzun
 * @since 11/12/14 3:39 PM
 */
public class WriteFileException extends BaseException {

    public WriteFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
