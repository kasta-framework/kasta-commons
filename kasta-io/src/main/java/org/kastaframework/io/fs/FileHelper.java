package org.kastaframework.io.fs;

/**
 * File helper.
 *
 * @author Sabri Onur Tuzun
 * @since 11/12/14 2:48 PM
 */
public interface FileHelper {

    /**
     * Deletes file from path.
     *
     * @param path path
     */
    void deleteFile(String path);

    /**
     * Deletes file under parent file.
     *
     * @param parent parent
     * @param path   path
     */
    void deleteFile(String parent, String path);

    /**
     * Writes byte array to a file.
     *
     * @param path  path
     * @param bytes bytes
     */
    void writeFile(String path, byte[] bytes);

    /**
     * Copies temp file to dest file path.
     *
     * @param tempFilePath tempFilePath
     * @param destDirPath  destDirPath
     */
    void copyFileToDirectory(String tempFilePath, String destDirPath);

    /**
     * Copies temp file to dest file path.
     *
     * @param tempFilePath tempFilePath
     * @param destFilePath destFilePath
     */
    void copyFileToFile(String tempFilePath, String destFilePath);

    /**
     * Appends extension to destination file path and copies it.
     *
     * @param tempFilePath tempFilePath
     * @param destBasePath destBasePath
     * @param destFileName destFileName
     * @return dest file
     */
    String appendExtensionAndCopyFileToFile(String tempFilePath, String destBasePath, String destFileName);

    /**
     * Returns file extension.
     *
     * @param path path
     * @return extension
     */
    String getFileExtension(String path);
}
