package org.kastaframework.io;

import java.lang.reflect.Method;

/**
 * Creates class method.
 *
 * @author Sabri Onur Tuzun
 * @since 05.08.2014 11:03
 */
public interface ClassMethodFinder {

    /**
     * Creates class method using object arguments.
     *
     * @return method
     */
    Method createMethod();
}
