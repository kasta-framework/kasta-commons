package org.kastaframework.io;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;

/**
 * Reflection utility class.
 *
 * @author Sabri Onur Tuzun
 * @since 05.01.2014 18:25
 */
public class ReflectionHelperImpl implements ReflectionHelper {

    /**
     * Invokes method.
     *
     * @param method method
     * @param target target
     * @param args   args
     * @return value
     */
    public Object invokeMethod(Method method, Object target, Object... args) {
        return ReflectionUtils.invokeMethod(method, target, args);
    }

    /**
     * Finds method.
     *
     * @param clazz      class
     * @param name       name
     * @param paramTypes parameter types
     * @param <T>        type
     * @return method
     */
    public <T> Method findMethod(Class<T> clazz, String name, Class<?>... paramTypes) {
        return ReflectionUtils.findMethod(clazz, name, paramTypes);
    }
}
