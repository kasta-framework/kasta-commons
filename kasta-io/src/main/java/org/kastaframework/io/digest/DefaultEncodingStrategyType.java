package org.kastaframework.io.digest;

/**
 * @author Sabri Onur Tuzun
 * @since 11/3/14 6:16 PM
 */
public enum DefaultEncodingStrategyType implements EncodingStrategyType {
    Md5("MD5"),
    Sha256("SHA256");

    private final String value;

    private DefaultEncodingStrategyType(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
