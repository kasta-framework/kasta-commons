package org.kastaframework.io.digest;

import org.kastaframework.collections.ValueEnum;

/**
 * Encoding strategy type.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 11:23 AM
 */
public interface EncodingStrategyType extends ValueEnum<String> {
}
