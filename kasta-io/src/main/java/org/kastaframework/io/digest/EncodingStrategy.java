package org.kastaframework.io.digest;

/**
 * Encoding strategy.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 11:15 AM
 */
public interface EncodingStrategy {

    /**
     * Encodes given data with encoding strategy.
     *
     * @param data data
     * @return encoded string
     */
    String encode(String data);
}
