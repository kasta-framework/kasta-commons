package org.kastaframework.io.digest;

/**
 * Factory for encoding strategies.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 11:22 AM
 */
public interface EncodingStrategyFactory {

    /**
     * Finds encoding strategy with its type.
     *
     * @param type type
     * @return encoding strategy
     */
    EncodingStrategy getEncodingStrategy(EncodingStrategyType type);

    /**
     * Encodes given data with encoding strategy.
     *
     * @param data data
     * @return encodede string
     */
    String encode(EncodingStrategyType type, String data);
}
