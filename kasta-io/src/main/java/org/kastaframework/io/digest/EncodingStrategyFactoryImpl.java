package org.kastaframework.io.digest;

import java.util.Map;

/**
 * Encoding strategy factory implementation.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 11:25 AM
 */
public class EncodingStrategyFactoryImpl implements EncodingStrategyFactory {

    private Map<EncodingStrategyType, EncodingStrategy> encodingStrategies;

    /**
     * Constructor with strategies.
     *
     * @param encodingStrategies encoding strategies
     */
    public EncodingStrategyFactoryImpl(Map<EncodingStrategyType, EncodingStrategy> encodingStrategies) {
        this.encodingStrategies = encodingStrategies;
    }

    /**
     * Returns strategies.
     *
     * @return encoding  strategies.
     */
    public Map<EncodingStrategyType, EncodingStrategy> getEncodingStrategies() {
        return encodingStrategies;
    }

    /**
     * Finds encoding strategy with its type.
     *
     * @param type type
     * @return encoding strategy
     */
    @Override
    public EncodingStrategy getEncodingStrategy(EncodingStrategyType type) {
        return encodingStrategies.get(type);
    }

    /**
     * Encodes given data with encoding strategy.
     *
     * @param data data
     * @return encodede string
     */
    @Override
    public String encode(EncodingStrategyType type, String data) {
        return getEncodingStrategy(type).encode(data);
    }
}
