package org.kastaframework.io.digest;

import java.util.Random;

/**
 * Key generator class.
 *
 * @author Sabri Onur Tuzun
 */
public class KeyGenerator {

    private static Random r = new Random();
    private static char[] charSet = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n',
            'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '2', '3', '4', '5', '6', '7', '8', '9'
    };

    /**
     * Generates random key with given length.
     *
     * @param length length
     * @return random generated key
     */
    public String createKey(int length) {
        if (length < 1) {
            throw new IllegalArgumentException("Illegal key length " + length);
        }
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < length; i++) {
            buffer.append(charSet[r.nextInt(charSet.length)]);
        }
        return buffer.toString();
    }
}
