package org.kastaframework.io.digest;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Md5 based encoding strategy.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 11:19 AM
 */
public class Md5EncodingStrategy implements EncodingStrategy {

    /**
     * Encodes given data with md5 algorithm.
     *
     * @param data data
     * @return encoded string
     */
    @Override
    public String encode(String data) {
        return DigestUtils.md5Hex(data);
    }
}
