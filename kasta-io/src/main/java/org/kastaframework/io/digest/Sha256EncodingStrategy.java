package org.kastaframework.io.digest;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Sha256 based encoding strategy.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 11:17 AM
 */
public class Sha256EncodingStrategy implements EncodingStrategy {

    /**
     * Encodes given data with sha256 algorithm.
     *
     * @param data data
     * @return encoded string
     */
    @Override
    public String encode(String data) {
        return DigestUtils.sha256Hex(data);
    }
}
