package org.kastaframework.io.digest;

/**
 * Encoding utility class which provides utility functions
 * for encoding strategies.
 *
 * @author Sabri Onur Tuzun
 * @since 10/12/14 6:51 PM
 */
public class EncodingUtils {

    private static EncodingStrategyFactory encodingStrategyFactory;

    /**
     * Encodes given string with encoding strategy.
     *
     * @param type encoding strategy type
     * @param data data
     * @return encoded string
     */
    public static String encode(EncodingStrategyType type, String data) {
        return encodingStrategyFactory.encode(type, data);
    }

    /**
     * Encodes given string with sha256 encoding strategy.
     *
     * @param data data
     * @return encoded string
     */
    public static String encodeSha256(String data) {
        return encode(DefaultEncodingStrategyType.Sha256, data);
    }

    /**
     * Encodes given string with md5 encoding strategy.
     *
     * @param data data
     * @return encoded string
     */
    public static String encodeMd5(String data) {
        return encode(DefaultEncodingStrategyType.Md5, data);
    }

    /**
     * Sets encodingStrategyFactory.
     *
     * @param encodingStrategyFactory encodingStrategyFactory
     */
    public void setEncodingStrategyFactory(EncodingStrategyFactory encodingStrategyFactory) {
        EncodingUtils.encodingStrategyFactory = encodingStrategyFactory;
    }
}
