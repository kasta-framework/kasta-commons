package org.kastaframework.io;

import java.lang.reflect.Method;

/**
 * Class method finder that uses reflection helper.
 *
 * @author Sabri Onur Tuzun
 * @since 05.08.2014 11:03
 */
public class ClassMethodFinderImpl implements ClassMethodFinder {

    private Class<?> clazz;
    private String methodName;
    private Class<?>[] paramTypes;
    private ReflectionHelper reflectionHelper;

    /**
     * Constructor to build factory instance.
     *
     * @param reflectionHelper reflectionHelper
     * @param clazz            clazz
     * @param methodName       methodName
     * @param paramTypes       paramTypes
     */
    public ClassMethodFinderImpl(ReflectionHelper reflectionHelper, Class<?> clazz, String methodName, Class<?>[] paramTypes) {
        this.reflectionHelper = reflectionHelper;
        this.clazz = clazz;
        this.methodName = methodName;
        this.paramTypes = paramTypes;
    }

    /**
     * Constructor to build factory instance.
     *
     * @param reflectionHelper reflectionHelper
     * @param clazz            clazz
     * @param methodName       methodName
     */
    public ClassMethodFinderImpl(ReflectionHelper reflectionHelper, Class<?> clazz, String methodName) {
        this(reflectionHelper, clazz, methodName, null);
    }

    /**
     * Creates class method by reflection.
     *
     * @return method
     */
    @Override
    public Method createMethod() {
        return reflectionHelper.findMethod(clazz, methodName, paramTypes);
    }
}
