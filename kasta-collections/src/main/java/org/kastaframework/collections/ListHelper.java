package org.kastaframework.collections;

import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

/**
 * List helper.
 *
 * @author Sabri Onur Tuzun
 * @since 11/8/14 1:44 AM
 */
public final class ListHelper {

    /**
     * Constructs new linked list with elements.
     *
     * @param elements elements
     * @return new linked list
     */
    public static List<Object> newLinkedList(Object... elements) {
        if (ArrayHelper.isEmpty(elements)) return null;
        List<Object> list = Lists.newLinkedList();
        Collections.addAll(list, elements);
        return list;
    }

    /**
     * Checks if given list is not empty and null.
     *
     * @param list list
     * @param <T>  list element class type
     * @return true is not empty and null
     */
    public static <T> boolean isNotEmpty(List<T> list) {
        return (list != null && list.size() > 0);
    }

    /**
     * Checks if given list is empty or null.
     *
     * @param list list
     * @param <T>  list element class type
     * @return true is empty or null
     */
    public static <T> boolean isEmpty(List<T> list) {
        return !isNotEmpty(list);
    }
}
