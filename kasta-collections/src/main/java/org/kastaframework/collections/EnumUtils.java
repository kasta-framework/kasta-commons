package org.kastaframework.collections;

/**
 * Handy class for enum utilities.
 *
 * @author Sabri Onur Tuzun
 * @since 25.09.2013 10:04
 */
public final class EnumUtils {

    /*
     * Private constructor.
     */
    private EnumUtils() {
    }

    /**
     * Returns ValueEnum typed enum class by looking at its value.
     *
     * @param <T>   class type
     * @param <V>   value
     * @param clazz class
     * @param value value
     * @return enum
     */
    public static <T extends Enum<T> & ValueEnum<V>, V> T getValueEnum(Class<T> clazz, V value) {
        T t = null;
        T[] enums = clazz.getEnumConstants();

        if (enums != null && enums.length > 0) {
            for (T enumType : enums) {
                if (enumType.getValue().equals(value)) {
                    t = enumType;
                    break;
                }
            }
        }
        return t;
    }

    /**
     * Returns KeyValueEnum typed enum class by looking at its key.
     *
     * @param <T>   class type
     * @param <K>   key
     * @param <V>   value
     * @param clazz class
     * @param key   key
     * @return enum
     */
    public static <T extends Enum<T> & KeyValueEnum<K, V>, K, V> T getKeyValueEnum(Class<T> clazz, K key) {
        T t = null;
        T[] enums = clazz.getEnumConstants();

        if (enums != null && enums.length > 0) {
            for (T enumType : enums) {
                if (enumType.getKey().equals(key)) {
                    t = enumType;
                    break;
                }
            }
        }
        return t;
    }

    /**
     * Returns value of KeyValueEnum class by looking at its key.
     *
     * @param <T>   class type
     * @param <K>   key
     * @param <V>   value
     * @param clazz class
     * @param key   key
     * @return value
     */
    public static <T extends Enum<T> & KeyValueEnum<K, V>, K, V> V getValue(Class<T> clazz, K key) {
        V value = null;
        T keyValueEnum = getKeyValueEnum(clazz, key);

        if (keyValueEnum != null) {
            value = keyValueEnum.getValue();
        }
        return value;
    }
}
