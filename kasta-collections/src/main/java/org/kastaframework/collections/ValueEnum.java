package org.kastaframework.collections;

/**
 * ValueEnum class.
 *
 * @author Sabri Onur Tuzun
 * @since 25.09.2013 10:04
 */
public interface ValueEnum<V> {

    /**
     * Returns value.
     *
     * @return value
     */
    V getValue();
}
