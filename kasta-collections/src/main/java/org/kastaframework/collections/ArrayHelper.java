package org.kastaframework.collections;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * Utility class for arrays.
 *
 * @author Sabri Onur Tuzun
 * @since 08.11.2013 19:16
 */
public final class ArrayHelper {

    public static final int MIN_ARRAY_PAIR_LENGTH = 1;

    /**
     * Check if given array is empty.
     *
     * @param arr array
     * @return true is empty or null
     */
    public static boolean isEmpty(Object[] arr) {
        return ArrayUtils.isEmpty(arr);
    }

    /**
     * Checks if given array is not empty.
     *
     * @param arr array
     * @return true if not empty
     */
    public static boolean isNotEmpty(Object[] arr) {
        return ArrayUtils.isNotEmpty(arr);
    }

    /**
     * Checks if given array is empty inside.
     *
     * @param values values
     * @return true if empty at all
     */
    public static boolean isNullSafe(Object... values) {
        if (isEmpty(values)) return false;
        for (Object val : values) {
            if (val == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if array contains length is greater than given parameter.
     *
     * @param arr    array
     * @param length length
     * @return true if array contains length is greater than given parameter
     */
    public static boolean hasLengthGreaterThan(Object[] arr, int length) {
        try {
            return (isNotEmpty(arr) && arr.length > length);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Splits array element with given separator and
     * generates key value map after split opeation.
     *
     * @param array          string array
     * @param separatorChars separator chars
     * @return key value map
     */
    public static Map<String, String> splitAndMap(String[] array, String separatorChars) {
        if (ArrayHelper.isEmpty(array)) return null;
        Map<String, String> keyValueMap = Maps.newHashMap();
        for (String element : array) {
            String[] pair = StringUtils.split(element, separatorChars);
            if (ArrayHelper.hasLengthGreaterThan(pair, MIN_ARRAY_PAIR_LENGTH)) {
                keyValueMap.put(pair[0], pair[1]);
            }
        }
        return keyValueMap;
    }

    /**
     * Converts list to array.
     *
     * @param list list
     * @return array
     */
    public static Object[] toArray(List<Object> list) {
        if (list == null) return null;
        return list.toArray(new Object[list.size()]);
    }
}
