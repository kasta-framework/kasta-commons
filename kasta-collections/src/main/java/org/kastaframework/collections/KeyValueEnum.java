package org.kastaframework.collections;

/**
 * KeyValueEnum class.
 *
 * @author Sabri Onur Tuzun
 * @since 25.09.2013 10:04
 */
public interface KeyValueEnum<K, V> extends ValueEnum<V> {

    /**
     * Returns key.
     *
     * @return key
     */
    K getKey();
}
