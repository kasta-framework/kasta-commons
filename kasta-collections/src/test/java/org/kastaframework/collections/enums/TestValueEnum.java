package org.kastaframework.collections.enums;

import org.kastaframework.collections.ValueEnum;

/**
 * @author Sabri Onur Tuzun
 * @since 09.09.2013 09:17
 */
public enum TestValueEnum implements ValueEnum<String> {
    TEST1("test1");

    private final String value;

    private TestValueEnum(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
