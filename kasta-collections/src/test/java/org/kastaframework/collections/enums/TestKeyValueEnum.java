package org.kastaframework.collections.enums;

import org.kastaframework.collections.KeyValueEnum;

/**
 * @author Sabri Onur Tuzun
 * @since 09.09.2013 09:17
 */
public enum TestKeyValueEnum implements KeyValueEnum<Integer, String> {
    TEST1(1, "test1");

    private final int key;
    private final String value;

    private TestKeyValueEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public Integer getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }
}
