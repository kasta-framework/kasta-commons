package org.kastaframework.collections.enums;

import org.junit.Assert;
import org.junit.Test;
import org.kastaframework.collections.EnumUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

/**
 * @author Sabri Onur Tuzun
 * @since 09.09.2013 09:02
 */
public class EnumUtilsTest {

    @Test
    public void testEnumUtilsPrivateConstructor() throws Exception {
        Constructor constructor = EnumUtils.class.getDeclaredConstructor();
        Assert.assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void testValueEnumReturningNullObject() {
        TestValueEnum valueEnum = EnumUtils.getValueEnum(TestValueEnum.class, "test3");
        Assert.assertNull(valueEnum);
    }

    @Test
    public void testValueEnumReturningValueEnum() {
        TestValueEnum valueEnum = EnumUtils.getValueEnum(TestValueEnum.class, "test1");
        Assert.assertNotNull(valueEnum);
        Assert.assertEquals(TestValueEnum.TEST1, valueEnum);
        Assert.assertEquals(TestValueEnum.TEST1.getValue(), valueEnum.getValue());
    }

    @Test
    public void testKeyValueEnumReturningNullObject() {
        TestKeyValueEnum keyValueEnum = EnumUtils.getKeyValueEnum(TestKeyValueEnum.class, 3);
        Assert.assertNull(keyValueEnum);
    }

    @Test
    public void testKeyValueEnumValueReturningNullValue() {
        String value = EnumUtils.getValue(TestKeyValueEnum.class, 3);
        Assert.assertNull(value);
    }

    @Test
    public void testKeyValueEnumValueReturningValue() {
        String value = EnumUtils.getValue(TestKeyValueEnum.class, 1);
        Assert.assertNotNull(value);
        Assert.assertEquals(TestKeyValueEnum.TEST1.getValue(), value);
    }

    @Test
    public void testKeyValueEnumReturningKeyValueEnum() {
        TestKeyValueEnum keyValueEnum = EnumUtils.getKeyValueEnum(TestKeyValueEnum.class, 1);
        Assert.assertNotNull(keyValueEnum);
        Assert.assertEquals(TestKeyValueEnum.TEST1, keyValueEnum);
        Assert.assertEquals(TestKeyValueEnum.TEST1.getKey(), keyValueEnum.getKey());
        Assert.assertEquals(TestKeyValueEnum.TEST1.getValue(), keyValueEnum.getValue());
    }
}
