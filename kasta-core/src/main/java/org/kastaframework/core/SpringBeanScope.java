package org.kastaframework.core;

import org.kastaframework.collections.ValueEnum;

/**
 * Spring bean scopes.
 *
 * @author Sabri Onur Tuzun
 * @since 05.08.2014 12:04
 */
public enum SpringBeanScope implements ValueEnum<String> {
    SINGLETON("singleton"),
    PROTOTYPE("prototype");

    private final String value;

    private SpringBeanScope(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
