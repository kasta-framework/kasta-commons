package org.kastaframework.core.exception;

/**
 * Base runtime exception.
 *
 * @author Sabri Onur Tuzun
 * @since 26.10.2013 16:53
 */
public abstract class BaseException extends RuntimeException {

    /**
     * Constructor with message.
     *
     * @param message message
     */
    protected BaseException(String message) {
        super(message);
    }

    /**
     * Constructor with message and throwable cause.
     *
     * @param message message
     * @param cause   cause
     */
    protected BaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
