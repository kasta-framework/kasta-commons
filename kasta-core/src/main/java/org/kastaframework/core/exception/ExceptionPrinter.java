package org.kastaframework.core.exception;

/**
 * Handles printing exception messages.
 *
 * @author Sabri Onur Tuzun
 * @since 11/6/14 3:07 PM
 */
public interface ExceptionPrinter {

    /**
     * Prints exception messages.
     *
     * @param exception exception
     * @return exception message
     */
    String print(Exception exception);
}
