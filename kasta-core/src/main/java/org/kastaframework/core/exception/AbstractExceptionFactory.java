package org.kastaframework.core.exception;

import org.kastaframework.core.reflect.DefaultBasedConditionalClassInstanceInstantiator;
import org.kastaframework.core.reflect.InstanceInstantiator;
import org.kastaframework.core.reflect.InstantiatorCondition;

/**
 * Base exception factory.
 *
 * @author Sabri Onur Tuzun
 * @since 16.05.2014 21:19
 */
public abstract class AbstractExceptionFactory implements ExceptionFactory {

    private ThrowableExceptionInstanceBuilder exceptionInstanceBuilder;

    /**
     * Throws exception using instantiator object.
     *
     * @param instantiator   instantiator
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            type
     * @throws T
     */
    protected <T extends Exception> void throwException(InstanceInstantiator<T> instantiator, Class<T> exceptionClass, Object... exceptionArgs) throws T {
        exceptionInstanceBuilder.buildAndThrowException(instantiator, exceptionClass, exceptionArgs);
    }

    /**
     * Throws exception using instantiator condition.
     * This method creates default based class instantiator.
     *
     * @param condition      condition
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            type
     * @throws T
     */
    protected <T extends Exception> void throwException(InstantiatorCondition condition, Class<T> exceptionClass, Object... exceptionArgs) throws T {
        InstanceInstantiator<T> instantiator = new DefaultBasedConditionalClassInstanceInstantiator<>(condition);
        throwException(instantiator, exceptionClass, exceptionArgs);
    }

    /**
     * Sets exception instance builder.
     *
     * @param exceptionInstanceBuilder exceptionInstanceBuilder
     */
    public void setExceptionInstanceBuilder(ThrowableExceptionInstanceBuilder exceptionInstanceBuilder) {
        this.exceptionInstanceBuilder = exceptionInstanceBuilder;
    }
}
