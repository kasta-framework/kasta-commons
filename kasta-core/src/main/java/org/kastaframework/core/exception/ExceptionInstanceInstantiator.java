package org.kastaframework.core.exception;

import org.kastaframework.core.reflect.InstanceInstantiator;

/**
 * Exception instance instantiator.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 20:28
 */
public interface ExceptionInstanceInstantiator {

    /**
     * Instantiates exception instance.
     *
     * @param instantiator   instantiator
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            type
     * @return exception instance
     */
    <T extends Exception> T instantiateException(InstanceInstantiator<T> instantiator, Class<T> exceptionClass, Object... exceptionArgs);
}
