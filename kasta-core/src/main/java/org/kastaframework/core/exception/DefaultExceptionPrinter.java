package org.kastaframework.core.exception;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Default exception printer.
 *
 * @author Sabri Onur Tuzun
 * @since 11/6/14 3:08 PM
 */
public class DefaultExceptionPrinter implements ExceptionPrinter {

    /**
     * Appends exception message and exception message cause.
     *
     * @param exception exception
     * @return exception message
     */
    @Override
    public String print(Exception exception) {
        StringBuilder message = new StringBuilder();
        if (StringUtils.isNotBlank(exception.getMessage())) {
            message.append("Message: ");
            message.append(ExceptionUtils.getMessage(exception));
        }
        String rootCauseMessage = ExceptionUtils.getRootCauseMessage(exception);
        if (StringUtils.isNotBlank(rootCauseMessage)) {
            message.append("Message root cause: ");
            message.append(rootCauseMessage);
        }
        return message.toString();
    }
}
