package org.kastaframework.core.exception;

import java.util.Collection;

/**
 * Exception factory helper that enables
 * static method invocation.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 19:25
 */
public class ExceptionFactoryHelper {

    private static ExceptionFactory exceptionFactory;

    /**
     * Delegates its logic to exception factory.
     *
     * @param obj            obj
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            type
     * @throws T
     */
    public static <T extends Exception> void throwIfNull(Object obj, Class<T> exceptionClass, Object... exceptionArgs) throws T {
        exceptionFactory.throwIfNull(obj, exceptionClass, exceptionArgs);
    }

    /**
     * Delegates its logic to exception factory.
     *
     * @param collection     collection
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            exception class type
     * @param <E>            collection element type
     * @throws T
     */
    public static <T extends Exception, E> void throwIfNotEmpty(Collection<E> collection, Class<T> exceptionClass, Object... exceptionArgs) throws T {
        exceptionFactory.throwIfNotEmpty(collection, exceptionClass, exceptionArgs);
    }

    /**
     * Sets exception factory.
     *
     * @param exceptionFactory exceptionFactory
     */
    public void setExceptionFactory(ExceptionFactory exceptionFactory) {
        ExceptionFactoryHelper.exceptionFactory = exceptionFactory;
    }
}
