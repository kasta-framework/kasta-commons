package org.kastaframework.core.exception;

import org.kastaframework.core.reflect.InstanceInstantiator;

/**
 * Implementation of throwable exception instance builder.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 20:38
 */
public class ThrowableExceptionInstanceBuilderImpl implements ThrowableExceptionInstanceBuilder {

    private ExceptionInstanceInstantiator instanceInstantiator;

    /**
     * Constructor with instantiator.
     *
     * @param instanceInstantiator instanceInstantiator
     */
    public ThrowableExceptionInstanceBuilderImpl(ExceptionInstanceInstantiator instanceInstantiator) {
        this.instanceInstantiator = instanceInstantiator;
    }

    /**
     * Returns exception instance instantiator.
     *
     * @return instantiator
     */
    public ExceptionInstanceInstantiator getInstanceInstantiator() {
        return instanceInstantiator;
    }

    /**
     * Builds exception and throws it.
     *
     * @param instantiator   instantiator
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            type
     * @throws T
     */
    @Override
    public <T extends Exception> void buildAndThrowException(InstanceInstantiator<T> instantiator, Class<T> exceptionClass, Object... exceptionArgs) throws T {
        T throwableException = instanceInstantiator.instantiateException(instantiator, exceptionClass, exceptionArgs);
        if (throwableException != null) {
            throw throwableException;
        }
    }
}
