package org.kastaframework.core.exception;

import org.kastaframework.core.reflect.InstanceInstantiator;
import org.kastaframework.core.reflect.InstanceInstantiatorFactory;

/**
 * Factory based exception instance instantiator.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 20:30
 */
public class ExceptionInstanceInstantiatorImpl implements ExceptionInstanceInstantiator {

    private InstanceInstantiatorFactory instantiatorFactory;

    /**
     * Constructor with instantiator factory.
     *
     * @param instantiatorFactory instantiatorFactory
     */
    public ExceptionInstanceInstantiatorImpl(InstanceInstantiatorFactory instantiatorFactory) {
        this.instantiatorFactory = instantiatorFactory;
    }

    /**
     * Instantiates and returns exception instance using
     * instantiator class.
     *
     * @param instantiator   instantiator
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            type
     * @return exception instance
     */
    @Override
    public <T extends Exception> T instantiateException(InstanceInstantiator<T> instantiator, Class<T> exceptionClass, Object... exceptionArgs) {
        return instantiatorFactory.instantiate(instantiator, exceptionClass, exceptionArgs);
    }
}
