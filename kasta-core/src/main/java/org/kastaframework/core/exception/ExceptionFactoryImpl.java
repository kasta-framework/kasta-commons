package org.kastaframework.core.exception;

import org.kastaframework.core.reflect.InstantiatorCondition;
import org.kastaframework.core.reflect.NotEmptyCollectionInstantiatorCondition;
import org.kastaframework.core.reflect.NullableObjectInstantiatorCondition;

import java.util.Collection;

/**
 * Exception factory which creates an exception.
 *
 * @author Sabri Onur Tuzun
 * @since 02.12.2013 09:37
 */
public class ExceptionFactoryImpl extends AbstractExceptionFactory {

    /**
     * Throws exception if object is not found.
     *
     * @param obj            object
     * @param exceptionClass exception class
     * @param exceptionArgs  exception arguments
     * @param <T>            type
     * @throws T
     */
    @Override
    public <T extends Exception> void throwIfNull(Object obj, Class<T> exceptionClass, Object... exceptionArgs) throws T {
        InstantiatorCondition condition = new NullableObjectInstantiatorCondition(obj);
        throwException(condition, exceptionClass, exceptionArgs);
    }

    /**
     * Throws exception if given collection is not empty.
     *
     * @param collection     collection
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            exception class type
     * @param <E>            collection element type
     * @throws T
     */
    @Override
    public <T extends Exception, E> void throwIfNotEmpty(Collection<E> collection, Class<T> exceptionClass, Object... exceptionArgs) throws T {
        InstantiatorCondition condition = new NotEmptyCollectionInstantiatorCondition<>(collection);
        throwException(condition, exceptionClass, exceptionArgs);
    }
}
