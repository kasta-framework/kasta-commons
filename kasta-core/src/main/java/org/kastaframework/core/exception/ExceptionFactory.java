package org.kastaframework.core.exception;

import java.util.Collection;

/**
 * Exception factory which creates an exception.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 18:42
 */
public interface ExceptionFactory {

    /**
     * Throws exception if object is not found.
     *
     * @param obj            object
     * @param exceptionClass exception class
     * @param exceptionArgs  exception arguments
     * @param <T>            type
     * @throws T
     */
    <T extends Exception> void throwIfNull(Object obj, Class<T> exceptionClass, Object... exceptionArgs) throws T;

    /**
     * Throws exception if collection is not empty.
     *
     * @param collection     collection
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            exception class type
     * @param <E>            collection element type
     * @throws T
     */
    <T extends Exception, E> void throwIfNotEmpty(Collection<E> collection, Class<T> exceptionClass, Object... exceptionArgs) throws T;
}
