package org.kastaframework.core.exception;

import org.kastaframework.core.reflect.InstanceInstantiator;

/**
 * Throwable exception instance factory.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 20:37
 */
public interface ThrowableExceptionInstanceBuilder {

    /**
     * Throws exception.
     *
     * @param instantiator   instantiator
     * @param exceptionClass exceptionClass
     * @param exceptionArgs  exceptionArgs
     * @param <T>            type
     * @throws T
     */
    <T extends Exception> void buildAndThrowException(InstanceInstantiator<T> instantiator, Class<T> exceptionClass, Object... exceptionArgs) throws T;
}
