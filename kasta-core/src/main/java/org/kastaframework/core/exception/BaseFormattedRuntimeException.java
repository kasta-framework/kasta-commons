package org.kastaframework.core.exception;

import org.kastaframework.utils.FormatterUtils;

/**
 * Base formatted runtime exception which formats message
 * with given arguments.
 *
 * @author Sabri Onur Tuzun
 * @since 02.12.2013 10:28
 */
public abstract class BaseFormattedRuntimeException extends BaseException {

    private Object[] arguments;

    /**
     * Constructor with message and arguments.
     *
     * @param message   message
     * @param arguments arguments
     */
    protected BaseFormattedRuntimeException(String message, Object... arguments) {
        super(message);
        this.arguments = arguments;
    }

    /**
     * Constructor with message and cause.
     *
     * @param message   message
     * @param cause     cause
     * @param arguments arguments
     */
    protected BaseFormattedRuntimeException(String message, Throwable cause, Object... arguments) {
        super(message, cause);
        this.arguments = arguments;
    }

    /**
     * Returns arguments.
     *
     * @return arguments
     */
    public Object[] getArguments() {
        return arguments;
    }

    /**
     * Overrides getMessage method and formats leading parameters.
     *
     * @return message
     */
    @Override
    public String getMessage() {
        String message = super.getMessage();
        return FormatterUtils.format(message, arguments);
    }

    /**
     * Overrides getLocalizedMessage and formats leading parameters.
     *
     * @return localized message
     */
    @Override
    public String getLocalizedMessage() {
        String localizedMessage = super.getLocalizedMessage();
        return FormatterUtils.format(localizedMessage, arguments);
    }
}
