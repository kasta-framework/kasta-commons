package org.kastaframework.core;

/**
 * Object helper.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 14:48
 */
public final class Objects {

    public static final Object NULL_OBJECT = null;

    /**
     * Private constructor.
     */
    private Objects() {
    }
}
