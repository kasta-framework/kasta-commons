package org.kastaframework.core.resolver;

/**
 * Bean resolver.
 *
 * @author Sabri Onur Tuzun
 * @since 25.04.2014 15:42
 */
public interface BeanResolver {

    /**
     * Resolves bean by its name and casts
     * return value using given class.
     *
     * @param resolvingBeanName bean name that depends on resolver
     * @param beanClass         bean class
     * @param <T>               class type of bean
     * @return bean
     */
    <T> T resolveBean(String resolvingBeanName, Class<T> beanClass);
}
