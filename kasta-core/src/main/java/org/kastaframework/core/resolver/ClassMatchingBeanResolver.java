package org.kastaframework.core.resolver;

/**
 * Class matching bean resolver.
 *
 * @author Sabri Onur Tuzun
 * @since 27.04.2014 11:58
 */
public interface ClassMatchingBeanResolver extends BeanResolver {

    /**
     * Resolves bean by class name.
     *
     * @param classOfBeanMapKey class of bean map key
     * @param <T>               class type of bean class
     * @return bean
     */
    <T> T resolveBean(Class<T> classOfBeanMapKey);
}
