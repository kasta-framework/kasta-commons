package org.kastaframework.core.resolver;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;

/**
 * Base application context aware bean map.
 *
 * @author Sabri Onur Tuzun
 * @since 24.04.2014 17:14
 */
public abstract class AbstractApplicationContextAwareBeanResolver implements ApplicationContextAware, BeanResolver {

    protected ApplicationContext applicationContext;
    protected Map<String, String> beanNameMatchingMap;

    /**
     * Constructor with bean name matching.
     *
     * @param beanNameMatchingMap beanNameMatchingMap
     */
    protected AbstractApplicationContextAwareBeanResolver(Map<String, String> beanNameMatchingMap) {
        this.beanNameMatchingMap = beanNameMatchingMap;
    }

    /**
     * Resolves bean using bean descriptor.
     *
     * @param resolvingBeanName bean resolver name
     * @param beanClass         bean class
     * @param <T>               bean class type
     * @return spring bean
     */
    @Override
    public <T> T resolveBean(String resolvingBeanName, Class<T> beanClass) {
        String beanName = beanNameMatchingMap.get(resolvingBeanName);
        return applicationContext.getBean(beanName, beanClass);
    }

    /**
     * Returns bean name matching map.
     *
     * @return beanNameMatchingMap
     */
    public Object getBeanNameMatchingMap() {
        return beanNameMatchingMap;
    }

    /**
     * Sets application context.
     *
     * @param applicationContext applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
