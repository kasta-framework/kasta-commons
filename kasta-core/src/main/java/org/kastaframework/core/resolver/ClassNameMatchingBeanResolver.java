package org.kastaframework.core.resolver;

import java.util.Map;

/**
 * Class name matching bean resolver.
 *
 * @author Sabri Onur Tuzun
 * @since 28.04.2014 11:43
 */
public class ClassNameMatchingBeanResolver extends AbstractApplicationContextAwareBeanResolver implements ClassMatchingBeanResolver {

    /**
     * Constructor with beanNameMatchingMap.
     *
     * @param beanNameMatchingMap beanNameMatchingMap
     */
    public ClassNameMatchingBeanResolver(Map<String, String> beanNameMatchingMap) {
        super(beanNameMatchingMap);
    }

    /**
     * Finds bean descriptor by class name and resolves
     * bean from registry.
     *
     * @param classOfBeanMapKey class of bean map key
     * @param <T>               class type of bean class
     * @return bean
     */
    @Override
    public <T> T resolveBean(Class<T> classOfBeanMapKey) {
        return resolveBean(classOfBeanMapKey.getName(), classOfBeanMapKey);
    }
}
