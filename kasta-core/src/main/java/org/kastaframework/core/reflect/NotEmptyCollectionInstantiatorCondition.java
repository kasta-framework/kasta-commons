package org.kastaframework.core.reflect;

import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;

/**
 * Filled collection instantiator condition.
 *
 * @author Sabri Onur Tuzun
 * @since 24.04.2014 10:12
 */
public class NotEmptyCollectionInstantiatorCondition<E> implements InstantiatorCondition {

    private Collection<E> collection;

    /**
     * Default constructor.
     *
     * @param collection collection
     */
    public NotEmptyCollectionInstantiatorCondition(Collection<E> collection) {
        this.collection = collection;
    }

    /**
     * Returns collection.
     *
     * @return collection
     */
    public Collection<E> getCollection() {
        return collection;
    }

    /**
     * Passed if given collection is not empty.
     *
     * @return true if collection is not empty
     */
    @Override
    public boolean passed() {
        return CollectionUtils.isNotEmpty(collection);
    }
}
