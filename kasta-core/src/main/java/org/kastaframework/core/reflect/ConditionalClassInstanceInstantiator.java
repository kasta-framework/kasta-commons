package org.kastaframework.core.reflect;

/**
 * Conditional class instance instantiator.
 *
 * @author Sabri Onur Tuzun
 * @since 01.12.2013 23:00
 */
public interface ConditionalClassInstanceInstantiator<T> extends InstanceInstantiator<T> {

    /**
     * Returns default object if condition is not passed.
     *
     * @return default object
     */
    T getDefaultIfNotPassed();

    /**
     * Returns instantiator condition.
     *
     * @return condition
     */
    InstantiatorCondition getCondition();
}
