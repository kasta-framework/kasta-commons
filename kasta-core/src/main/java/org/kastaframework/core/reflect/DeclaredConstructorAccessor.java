package org.kastaframework.core.reflect;

import java.lang.reflect.Constructor;

/**
 * Constructor accessor that uses declared constructor.
 *
 * @author Sabri Onur Tuzun
 * @since 27.10.2013 18:42
 */
public class DeclaredConstructorAccessor<T> implements ConstructorAccessor<T> {

    private Class<T> accessibleClass;

    /**
     * Constructor.
     *
     * @param accessibleClass accessibleClass
     */
    public DeclaredConstructorAccessor(Class<T> accessibleClass) {
        this.accessibleClass = accessibleClass;
    }

    /**
     * Returns class
     *
     * @return class
     */
    public Class<T> getAccessibleClass() {
        return accessibleClass;
    }

    /**
     * Accesses constructor that uses declared constructor.
     *
     * @param parameterTypes parameterTypes
     * @return constructor
     */
    @Override
    public Constructor<T> access(Class... parameterTypes) {
        try {
            return accessibleClass.getDeclaredConstructor(parameterTypes);
        } catch (Exception e) {
            throw new NoSuchConstructorException("No such constructor is defined for class of: " + accessibleClass.getName(), e);
        }
    }
}
