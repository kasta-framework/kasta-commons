package org.kastaframework.core.reflect;

/**
 * This condition can pass if flag value is true and
 * object is null.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 17:23
 */
public class FlaggedNullableObjectInstantiatorCondition extends NullableObjectInstantiatorCondition {

    private boolean flag;

    /**
     * Constructor with object.
     *
     * @param flag   flag
     * @param object object
     */
    public FlaggedNullableObjectInstantiatorCondition(boolean flag, Object object) {
        super(object);
        this.flag = flag;
    }

    /**
     * Returns flag value.
     *
     * @return flag value
     */
    public boolean getFlag() {
        return flag;
    }

    /**
     * Passed if flag value is true and object is null.
     *
     * @return true if flag value is true and object is null
     */
    @Override
    public boolean passed() {
        return (flag && super.passed());
    }
}
