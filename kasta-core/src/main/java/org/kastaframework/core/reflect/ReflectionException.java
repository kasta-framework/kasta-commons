package org.kastaframework.core.reflect;

import org.kastaframework.core.exception.BaseException;

/**
 * All reflection exception has to be runtime.
 *
 * @author Sabri Onur Tuzun
 * @since 27.10.2013 17:00
 */
public class ReflectionException extends BaseException {

    /**
     * Constructor with message.
     *
     * @param message message
     */
    public ReflectionException(String message) {
        super(message);
    }

    /**
     * Constructor with message and cause.
     *
     * @param message message
     * @param cause   cause
     */
    public ReflectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
