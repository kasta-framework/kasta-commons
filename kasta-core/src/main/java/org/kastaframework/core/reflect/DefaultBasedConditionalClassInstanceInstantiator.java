package org.kastaframework.core.reflect;

/**
 * Default based conditional class intance instantiator.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 13:39
 */
public class DefaultBasedConditionalClassInstanceInstantiator<T> extends ClassInstanceInstantiator<T> implements ConditionalClassInstanceInstantiator<T> {

    private T defaultIfNotPassed;
    private InstantiatorCondition condition;

    /**
     * Constructor with default object setted.
     * Sets default object as null.
     *
     * @param condition condition
     */
    public DefaultBasedConditionalClassInstanceInstantiator(InstantiatorCondition condition) {
        this(null, condition);
    }

    /**
     * Default constructor.
     *
     * @param defaultIfNotPassed defaultIfNotPassed
     * @param condition          condition
     */
    public DefaultBasedConditionalClassInstanceInstantiator(T defaultIfNotPassed, InstantiatorCondition condition) {
        this.defaultIfNotPassed = defaultIfNotPassed;
        this.condition = condition;
    }

    /**
     * Returns default object if condition is not passed.
     * This object can be null or something.
     *
     * @return default object
     */
    @Override
    public T getDefaultIfNotPassed() {
        return defaultIfNotPassed;
    }

    /**
     * Returns instantiator condition.
     *
     * @return condition
     */
    @Override
    public InstantiatorCondition getCondition() {
        return condition;
    }

    /**
     * First checks condition if passed, return default object
     * if not passed.
     *
     * @param accessor  accessor
     * @param invoker   invoker
     * @param arguments arguments
     * @return instance
     */
    @Override
    public T instantiate(ConstructorAccessor<T> accessor, ConstructorInvoker<T> invoker, Object... arguments) {
        return condition.passed() ? super.instantiate(accessor, invoker, arguments) : defaultIfNotPassed;
    }
}
