package org.kastaframework.core.reflect;

/**
 * Instance instantiator factory.
 *
 * @author Sabri Onur Tuzun
 * @since 28.10.2013 02:17
 */
public class DefaultInstanceInstantiatorFactory implements InstanceInstantiatorFactory {

    /**
     * Instantiates default contructor. This method
     * delegates its logic.
     *
     * @param clazz class
     * @param <T>   type
     * @return object
     */
    @Override
    public <T> T instantiateDefault(Class<T> clazz) {
        return instantiate(clazz);
    }

    /**
     * Instantiates constructor using default instantiator.
     * Uses ClassInstanceInstantiator to instantiate.
     *
     * @param clazz     class
     * @param arguments arguments
     * @param <T>       type
     * @return object
     */
    @Override
    public <T> T instantiate(Class<T> clazz, Object... arguments) {
        InstanceInstantiator<T> instantiator = new ClassInstanceInstantiator<>();
        return instantiate(instantiator, clazz, arguments);
    }

    /**
     * Instantiates default constructor. This method
     * delegates its logic.
     *
     * @param instantiator instantiator
     * @param clazz        class
     * @param <T>          type
     * @return object
     */
    @Override
    public <T> T instantiateDefault(InstanceInstantiator<T> instantiator, Class<T> clazz) {
        return instantiate(instantiator, clazz);
    }

    /**
     * Instantites constructor using instantiator and arguments.
     * Constructor accessor and invoker classes are used to
     * instantiate instance.
     *
     * @param instantiator instantiator
     * @param clazz        class
     * @param arguments    arguments
     * @param <T>          type
     * @return object
     */
    @Override
    public <T> T instantiate(InstanceInstantiator<T> instantiator, Class<T> clazz, Object... arguments) {
        ConstructorAccessor<T> accessor = new DeclaredConstructorAccessor<>(clazz);
        ConstructorInvoker<T> invoker = new ConstructorInvokerImpl<>();
        return instantiate(instantiator, accessor, invoker, arguments);
    }

    /**
     * Instantiates default constructor using constructor interfaces.
     * This method delegates its logic.
     *
     * @param instantiator instantiator
     * @param accessor     accessor
     * @param invoker      invoker
     * @param <T>          type
     * @return object
     */
    @Override
    public <T> T instantiateDefault(InstanceInstantiator<T> instantiator, ConstructorAccessor<T> accessor, ConstructorInvoker<T> invoker) {
        return instantiate(instantiator, accessor, invoker);
    }

    /**
     * Instantiates constructor using constructor interfaces.
     *
     * @param instantiator instantiator
     * @param accessor     accessor
     * @param invoker      instantiator
     * @param arguments    arguments
     * @param <T>          type
     * @return object
     */
    @Override
    public <T> T instantiate(InstanceInstantiator<T> instantiator, ConstructorAccessor<T> accessor, ConstructorInvoker<T> invoker, Object... arguments) {
        return instantiator.instantiate(accessor, invoker, arguments);
    }
}
