package org.kastaframework.core.reflect;

import java.lang.reflect.Constructor;

/**
 * Implementation of constructor invoker.
 *
 * @author Sabri Onur Tuzun
 * @since 27.10.2013 20:21
 */
public class ConstructorInvokerImpl<T> implements ConstructorInvoker<T> {

    /**
     * Invokes constructor.
     *
     * @param constructor constructor
     * @param arguments   arguments
     * @return object
     */
    @Override
    public T invoke(Constructor<T> constructor, Object... arguments) {
        try {
            return constructor.newInstance(arguments);
        } catch (Exception e) {
            throw new ConstructorInvocationException("Constructor cannot be invoked to a new instance", e);
        }
    }
}
