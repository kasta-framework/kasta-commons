package org.kastaframework.core.reflect;

/**
 * Constructor invocation exception.
 *
 * @author Sabri Onur Tuzun
 * @since 27.10.2013 20:41
 */
public class ConstructorInvocationException extends ReflectionException {

    /**
     * Constructor with message and cause.
     *
     * @param message message
     * @param cause   cause
     */
    public ConstructorInvocationException(String message, Throwable cause) {
        super(message, cause);
    }
}
