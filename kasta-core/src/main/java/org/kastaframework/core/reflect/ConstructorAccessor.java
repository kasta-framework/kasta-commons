package org.kastaframework.core.reflect;

import java.lang.reflect.Constructor;

/**
 * Constructor accessor.
 *
 * @author Sabri Onur Tuzun
 * @since 27.10.2013 16:29
 */
public interface ConstructorAccessor<T> {

    /**
     * Accesses constructor.
     *
     * @param parameterTypes parameterTypes
     * @return constructor
     */
    Constructor<T> access(Class... parameterTypes);
}
