package org.kastaframework.core.reflect;

/**
 * Instantiator condition that passed object is null.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 13:46
 */
public class NullableObjectInstantiatorCondition implements InstantiatorCondition {

    private Object object;

    /**
     * Constructor with object.
     *
     * @param object object
     */
    public NullableObjectInstantiatorCondition(Object object) {
        this.object = object;
    }

    /**
     * Checks if passed object is null.
     *
     * @return true if object is null
     */
    @Override
    public boolean passed() {
        return (object == null);
    }
}
