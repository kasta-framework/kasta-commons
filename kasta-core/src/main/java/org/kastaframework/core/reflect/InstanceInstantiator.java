package org.kastaframework.core.reflect;

/**
 * Instance instantiator.
 *
 * @author Sabri Onur Tuzun
 * @since 28.10.2013 00:46
 */
public interface InstanceInstantiator<T> {

    /**
     * Instantiates constructor with arguments.
     *
     * @param accessor  accessor
     * @param invoker   invoker
     * @param arguments arguments
     * @return object
     */
    T instantiate(ConstructorAccessor<T> accessor, ConstructorInvoker<T> invoker, Object... arguments);
}
