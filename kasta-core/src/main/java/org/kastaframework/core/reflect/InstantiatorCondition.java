package org.kastaframework.core.reflect;

/**
 * Instantiator condition.
 *
 * @author Sabri Onur Tuzun
 * @since 01.12.2013 23:01
 */
public interface InstantiatorCondition {

    /**
     * Checks if instance can be instantiated.
     *
     * @return true if passed
     */
    boolean passed();
}
