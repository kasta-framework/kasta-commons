package org.kastaframework.core.reflect;

import java.lang.reflect.Constructor;

/**
 * Constructor invoker.
 *
 * @author Sabri Onur Tuzun
 * @since 27.10.2013 17:52
 */
public interface ConstructorInvoker<T> {

    /**
     * Invokes constructor.
     *
     * @param constructor constructor
     * @param arguments   arguments
     * @return object
     */
    T invoke(Constructor<T> constructor, Object... arguments);
}
