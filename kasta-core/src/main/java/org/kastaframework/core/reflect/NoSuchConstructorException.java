package org.kastaframework.core.reflect;

/**
 * Runtime no such constructor exception.
 *
 * @author Sabri Onur Tuzun
 * @since 27.10.2013 16:57
 */
public class NoSuchConstructorException extends ReflectionException {

    /**
     * Constructor with message and cause.
     *
     * @param message message
     * @param cause   cause
     */
    public NoSuchConstructorException(String message, Throwable cause) {
        super(message, cause);
    }
}
