package org.kastaframework.core.reflect;

import org.apache.commons.lang3.ClassUtils;

import java.lang.reflect.Constructor;

/**
 * Class instance instantiater.
 *
 * @author Sabri Onur Tuzun
 * @since 28.10.2013 00:48
 */
public class ClassInstanceInstantiator<T> implements InstanceInstantiator<T> {

    /**
     * Instantiates constructor with arguments.
     *
     * @param accessor  accessor
     * @param invoker   invoker
     * @param arguments arguments
     * @return object
     */
    @Override
    public T instantiate(ConstructorAccessor<T> accessor, ConstructorInvoker<T> invoker, Object... arguments) {
        Constructor<T> constructor = accessor.access(ClassUtils.toClass(arguments));
        return invoker.invoke(constructor, arguments);
    }
}
