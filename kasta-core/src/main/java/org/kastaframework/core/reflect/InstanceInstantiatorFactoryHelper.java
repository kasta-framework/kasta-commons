package org.kastaframework.core.reflect;

/**
 * Instance instantiator factory helper which provides static access
 * to factory methods.
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 14:51
 */
public class InstanceInstantiatorFactoryHelper {

    private static InstanceInstantiatorFactory instantiatorFactory;

    /**
     * Delegates its logic to instantiator factory to instantiate
     * instance.
     *
     * @param clazz     class
     * @param arguments arguments
     * @param <T>       type
     * @return instance
     */
    public static <T> T instantiate(Class<T> clazz, Object... arguments) {
        return instantiatorFactory.instantiate(clazz, arguments);
    }

    /**
     * Delegates its logic to instantiator factory to instantiate
     * instance.
     *
     * @param instantiator instantiator
     * @param clazz        class
     * @param arguments    arguments
     * @param <T>          type
     * @return instance
     */
    public static <T> T instantiate(InstanceInstantiator<T> instantiator, Class<T> clazz, Object... arguments) {
        return instantiatorFactory.instantiate(instantiator, clazz, arguments);
    }

    /**
     * Sets instantiator factory.
     *
     * @param instantiatorFactory instantiatorFactory
     */
    public void setInstantiatorFactory(InstanceInstantiatorFactory instantiatorFactory) {
        InstanceInstantiatorFactoryHelper.instantiatorFactory = instantiatorFactory;
    }
}
