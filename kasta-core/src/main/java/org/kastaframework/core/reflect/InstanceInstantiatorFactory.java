package org.kastaframework.core.reflect;

/**
 * Instance instantiator factory
 *
 * @author Sabri Onur Tuzun
 * @since 23.04.2014 14:34
 */
public interface InstanceInstantiatorFactory {

    /**
     * Instantiates default contructor.
     *
     * @param clazz class
     * @param <T>   type
     * @return object
     */
    <T> T instantiateDefault(Class<T> clazz);

    /**
     * Instantiates constructor using default instantiator.
     *
     * @param clazz     class
     * @param arguments arguments
     * @param <T>       type
     * @return object
     */
    <T> T instantiate(Class<T> clazz, Object... arguments);

    /**
     * Instantiates default constructor.
     *
     * @param instantiator instantiator
     * @param clazz        class
     * @param <T>          type
     * @return object
     */
    <T> T instantiateDefault(InstanceInstantiator<T> instantiator, Class<T> clazz);

    /**
     * Instantites constructor using instantiator and arguments.
     *
     * @param instantiator instantiator
     * @param clazz        class
     * @param arguments    arguments
     * @param <T>          type
     * @return object
     */
    <T> T instantiate(InstanceInstantiator<T> instantiator, Class<T> clazz, Object... arguments);

    /**
     * Instantiates default constructor using constructor interfaces.
     *
     * @param instantiator instantiator
     * @param accessor     accessor
     * @param invoker      invoker
     * @param <T>          type
     * @return object
     */
    <T> T instantiateDefault(InstanceInstantiator<T> instantiator, ConstructorAccessor<T> accessor, ConstructorInvoker<T> invoker);

    /**
     * Instantiates constructor using constructor interfaces.
     *
     * @param instantiator instantiator
     * @param accessor     accessor
     * @param invoker      instantiator
     * @param arguments    arguments
     * @param <T>          type
     * @return object
     */
    <T> T instantiate(InstanceInstantiator<T> instantiator, ConstructorAccessor<T> accessor, ConstructorInvoker<T> invoker, Object... arguments);
}
