package org.kastaframework.core;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

/**
 * @author Sabri Onur Tuzun
 * @since 20.05.2014 14:33
 */
public class ObjectsTest {

    @Test
    public void testIfPrivateConstructorIsAccessible() throws Exception {
        Constructor constructor = Objects.class.getDeclaredConstructor();
        Assert.assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void testNullObject() {
        Assert.assertNull(Objects.NULL_OBJECT);
    }
}
