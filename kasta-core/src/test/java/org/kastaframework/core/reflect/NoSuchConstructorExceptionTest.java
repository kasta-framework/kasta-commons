package org.kastaframework.core.reflect;

import org.junit.Test;

/**
 * @author Sabri Onur Tuzun
 * @since 20.05.2014 14:54
 */
public class NoSuchConstructorExceptionTest {

    @Test(expected = NoSuchConstructorException.class)
    public void testThrowingOfExceptionInstance() {
        throw new NoSuchConstructorException("Caught exception", new RuntimeException());
    }
}
