package org.kastaframework.core.reflect;

import org.junit.Test;

/**
 * @author Sabri Onur Tuzun
 * @since 20.05.2014 14:54
 */
public class ConstructorInvocationExceptionTest {

    @Test(expected = ConstructorInvocationException.class)
    public void testThrowingOfExceptionInstance() {
        throw new ConstructorInvocationException("Caught exception", new RuntimeException());
    }
}
