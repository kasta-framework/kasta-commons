package org.kastaframework.core.exception;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Sabri Onur Tuzun
 * @since 19.05.2014 01:26
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:kasta-test-applicationContext-reflection-helper.xml",
        "classpath:kasta-test-applicationContext-exception-handler.xml"
})
public class ExceptionFactoryTest {

    @Autowired
    @Qualifier("testExceptionFactory")
    private ExceptionFactory exceptionFactory;

    @Test(expected = RuntimeException.class)
    public void testThrowingExceptionIfObjectNull() {
        exceptionFactory.throwIfNull(null, RuntimeException.class);
    }

    @Test(expected = RuntimeException.class)
    public void testThrowingExceptionIfCollectionNotEmpty() {
        exceptionFactory.throwIfNotEmpty(Arrays.asList("elem", "elem2"), RuntimeException.class);
    }

    @Test
    public void testThrowingExceptionIfNotNullObject() {
        exceptionFactory.throwIfNull(new Object(), RuntimeException.class);
    }

    @Test
    public void testThrowingExceptionIfCollectionNull() {
        exceptionFactory.throwIfNotEmpty(null, RuntimeException.class);
    }

    @Test
    public void testThrowingExceptionIfCollectionEmpty() {
        exceptionFactory.throwIfNotEmpty(new ArrayList<>(), RuntimeException.class);
    }
}
