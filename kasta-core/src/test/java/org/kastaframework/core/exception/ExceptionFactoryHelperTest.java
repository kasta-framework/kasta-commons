package org.kastaframework.core.exception;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Sabri Onur Tuzun
 * @since 20.05.2014 13:53
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:kasta-test-applicationContext-reflection-helper.xml",
        "classpath:kasta-test-applicationContext-exception-handler.xml"
})
public class ExceptionFactoryHelperTest {

    @Test(expected = RuntimeException.class)
    public void testThrowingExceptionIfObjectNull() {
        ExceptionFactoryHelper.throwIfNull(null, RuntimeException.class);
    }

    @Test(expected = RuntimeException.class)
    public void testThrowingExceptionIfCollectionNotEmpty() {
        ExceptionFactoryHelper.throwIfNotEmpty(Arrays.asList("elem", "elem2"), RuntimeException.class);
    }

    @Test
    public void testThrowingExceptionIfNotNullObject() {
        ExceptionFactoryHelper.throwIfNull(new Object(), RuntimeException.class);
    }

    @Test
    public void testThrowingExceptionIfCollectionNull() {
        ExceptionFactoryHelper.throwIfNotEmpty(null, RuntimeException.class);
    }

    @Test
    public void testThrowingExceptionIfCollectionEmpty() {
        ExceptionFactoryHelper.throwIfNotEmpty(new ArrayList<>(), RuntimeException.class);
    }
}
